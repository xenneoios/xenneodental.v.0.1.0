//
//  ViewController.swift
//  XENNEO-DENTAL
//
//  Created by XENNEO_iOS_2 on 20/03/18.
//  Copyright © 2018 XENNEO_iOS_2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var viewObjectSignUP:UIView!
    @IBOutlet var textFieldName:UITextField!
    @IBOutlet var textFieldEmail:UITextField!
    @IBOutlet var textFieldPassword:UITextField!
    @IBOutlet var textFieldCPassowrd:UITextField!
    @IBOutlet var buttonLogin:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
       // self.navigationController?.isNavigationBarHidden=true
        
        self.configureUI()
    }
    
    
    func configureUI(){
        viewObjectSignUP.layer.borderWidth=1.0
        viewObjectSignUP.layer.borderColor=UIColor.lightGray.cgColor;
        //  viewOjectSignUp.layer.borderWidth=1.0
        //   viewOjectSignUp.layer.borderColor=UIColor.lightGray.cgColor;
        // textFieldEmail.layer.borderWidth=1.0
        // textFieldEmail.layer.borderColor=UIColor.lightGray.cgColor;
        // textFieldPassword.layer.borderWidth=1.0
        // textFieldPassword.layer.borderColor=UIColor.lightGray.cgColor;
        self.setBottomBorderToTextField(textField: textFieldEmail)
        self.setBottomBorderToTextField(textField: textFieldPassword)
        self.setBottomBorderToTextField(textField: textFieldName)
        self.setBottomBorderToTextField(textField: textFieldCPassowrd)

        
        textFieldEmail.attributedPlaceholder = NSAttributedString(string: "Email Address",
                                                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        textFieldPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        textFieldName.attributedPlaceholder = NSAttributedString(string: "Name",
                                                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        textFieldCPassowrd.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
    
    }
    
    func setBottomBorderToTextField(textField:UITextField!){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        textField.backgroundColor=UIColor.clear
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

