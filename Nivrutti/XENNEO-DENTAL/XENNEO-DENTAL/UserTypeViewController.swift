//
//  UserTypeViewController.swift
//  XENNEO-DENTAL
//
//  Created by XENNEO_iOS_2 on 21/03/18.
//  Copyright © 2018 XENNEO_iOS_2. All rights reserved.
//

import UIKit

class UserTypeViewController: UIViewController {
    
    
    @IBOutlet var textFieldMyself:UITextField!
    @IBOutlet var textFieldSpouse:UITextField!
    @IBOutlet var textFieldChild:UITextField!
    @IBOutlet var textFieldOthers:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print("HI")
       self.configureUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureUI(){
        
        
        textFieldMyself.attributedPlaceholder = NSAttributedString(string: "Myself",
                                                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        textFieldSpouse.attributedPlaceholder = NSAttributedString(string: "Spouse",
                                                                     attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        textFieldChild.attributedPlaceholder = NSAttributedString(string: "Child",
                                                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        textFieldOthers.attributedPlaceholder = NSAttributedString(string: "Others",
                                                                   attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
