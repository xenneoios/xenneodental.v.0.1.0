//
//  BMIViewController.swift
//  XENNEO-DENTAL
//
//  Created by XENNEO_iOS_1 on 22/03/18.
//  Copyright © 2018 XENNEO_iOS_2. All rights reserved.
//

import UIKit


class BMIViewController: UIViewController {

    @IBOutlet weak var genderView: cardView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let segmentedControl = DPSegmentedControl.init(
           // FrameWithIcon: CGRectMake(8, 50, genderView.bounds.width - 16, 44),
            FrameWithIcon: CGRect(origin: .zero, size: CGSize(width: 100, height: 100)),

            items: ["Happy", "Normal"],
            icons: [UIImage(named: "happy_gray")!, UIImage(named: "flat_gray")!, UIImage(named: "sad_gray")!],
            selectedIcons: [UIImage(named: "Myself")!, UIImage(named: "Others")!],
            backgroundColor: UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1),
            thumbColor: hexStringToUIColor(hex: "#54C3EF"),
            textColor: hexStringToUIColor(hex: "#808080"),
            selectedTextColor: hexStringToUIColor(hex: "#FFFFFF"),
            orientation: ComponentOrientation.LeftRight)
        
        segmentedControl.selectedIndex = 1
        
       // segmentedControl.addTarget(self, action: #selector(self.action(_:)), forControlEvents: .ValueChanged)
        self.genderView.addSubview(segmentedControl)

        // Do any additional setup after loading the view.
    }
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
  
    


}
