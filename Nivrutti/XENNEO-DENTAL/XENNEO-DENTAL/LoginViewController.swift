//
//  LoginViewController.swift
//  XENNEO-DENTAL
//
//  Created by XENNEO_iOS_2 on 20/03/18.
//  Copyright © 2018 XENNEO_iOS_2. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet var viewOjectLogin:UIView!
    @IBOutlet var viewOjectSignUp:UIView!
    @IBOutlet var buttonLogin:UIButton!
    @IBOutlet var buttonSignUP:UIButton!
    @IBOutlet var textFieldName:UITextField!
    @IBOutlet var textFieldEmail:UITextField!
    @IBOutlet var textFieldPassword:UITextField!
    @IBOutlet var textFieldEmailSignUp:UITextField!
    @IBOutlet var textFieldPasswordSignUp:UITextField!
    @IBOutlet var textFieldCPassowrd:UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    self.configureUI()
        // Do any additional setup after loading the view.
    }
    
    func configureUI(){
    
        self.setBottomBorderToTextField(textField: textFieldEmail)
        self.setBottomBorderToTextField(textField: textFieldPassword)
        self.setBottomBorderToTextField(textField: textFieldName)
        self.setBottomBorderToTextField(textField: textFieldCPassowrd)
        self.setBottomBorderToTextField(textField: textFieldEmailSignUp)
        self.setBottomBorderToTextField(textField: textFieldPasswordSignUp)
        
        textFieldEmail.attributedPlaceholder = NSAttributedString(string: "Email",
                                                               attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        textFieldPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                  attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        
        textFieldName.attributedPlaceholder = NSAttributedString(string: "Name",
                                                                 attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        textFieldCPassowrd.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
                                                                      attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        
        viewOjectSignUp.isHidden=true
        viewOjectLogin.isHidden=false
        buttonLogin.setTitleColor(UIColor.red, for: .normal)
        buttonSignUP.setTitleColor(UIColor.darkGray, for: .normal)
        
       
        
    }
    
   
    
    func setBottomBorderToTextField(textField:UITextField!){
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.cgColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
        textField.backgroundColor=UIColor.clear
        
       
    }
    
    @IBAction func buttonLoginAndSignUP(_ sender: UIButton)
    {
        
        let number=sender.tag
        switch number {
        case 1:
            viewOjectLogin.isHidden=false
            viewOjectSignUp.isHidden=true
           
            buttonLogin.setTitleColor(UIColor.red, for: .normal)
            buttonSignUP.setTitleColor(UIColor.darkGray, for: .normal)
        case 2:
            viewOjectLogin.isHidden=true
            viewOjectSignUp.isHidden=false

            buttonLogin.setTitleColor(UIColor.darkGray, for: .normal)
            buttonSignUP.setTitleColor(UIColor.red, for: .normal)
            
            
        default:
            print("")
            
        }
        
    }
    
    
    @IBAction func buttonLoginTapped(){
        
        [self .performSegue(withIdentifier: "", sender: nil)];
        
        
       

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
