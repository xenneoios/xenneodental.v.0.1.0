//
//  cardView.swift
//  XenneoDental
//
//  Created by XENNEO_iOS_1 on 20/03/18.
//  Copyright © 2018 XENNEO_iOS_1. All rights reserved.
//

import UIKit

@IBDesignable class cardView: UIView {

    @IBInspectable var cornerradius : CGFloat = 2
    
   @IBInspectable var shadowoffsetwidth : CGFloat = 0
    
    @IBInspectable var shadowoffsetHeight : CGFloat = 5
    
    @IBInspectable var shadowColour : UIColor = UIColor.black
    
    @IBInspectable var shadowOpacity: CGFloat = 0.5



override func layoutSubviews() {
    
    layer.cornerRadius = cornerradius
    layer.shadowColor = shadowColour.cgColor
    layer.shadowOffset = CGSize(width: shadowoffsetwidth, height: shadowoffsetHeight)
    let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerradius)
    layer.shadowPath = shadowPath.cgPath
    layer.shadowOpacity = Float(shadowOpacity)
    
    }
}
